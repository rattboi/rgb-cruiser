{
  description = "rgb-cruiser";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, fenix }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";

        # use fenix to get a nightly comnpiler and pico compile target.
        mkToolchain = fenix.packages.${system}.combine;
        toolchain = fenix.packages.${system}.complete;
        target-arm = fenix.packages.${system}.targets."thumbv6m-none-eabi".latest;

        rustTargets = mkToolchain (with toolchain; [
          cargo
          rustc
          target-arm.rust-std

          clippy
        ]);

      in
        {
          # `nix develop`
          devShell = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [
              # our toolchain
              rustTargets

              picocom

              # -- use instead of elf2uf2-rs if you have a hardware debugger --
              probe-rs

              # -- needed if you want to compile probe-rs from source (cargo install) --
              # udev
              # pkgconfig
              # openssl.dev

              # -- extras for dev convenience -- 
              cargo-watch
              rustfmt
              rust-analyzer
              ];
          };
        }
    );
}
