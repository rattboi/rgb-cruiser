#![no_std]
#![no_main]
#![feature(async_closure)]

use core::option::Option::Some;

use defmt_rtt as _; // global logger
use panic_probe as _;

use embassy_executor::Spawner;
use embassy_time::Timer;

use embassy_stm32::Config;
use embassy_stm32::rcc::*;
use embassy_stm32::gpio::Pin;

use rgbcruiser::{backlight,menu,lcd,video_adc,switches};

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let mut config = Config::default();
    config.rcc.hsi = true;
    config.rcc.pll = Some(Pll {
        source: PllSource::HSI,
        div: PllDiv::DIV3,
        mul: PllMul::MUL6,
    });
    config.rcc.sys = Sysclk::PLL1_R;

    let p = embassy_stm32::init(config);

    spawner.spawn(backlight::backlight_task(p.PA10, p.TIM22)).unwrap();
    spawner.spawn(menu::up_button_task(p.PB3, p.EXTI3)).unwrap();
    spawner.spawn(menu::down_button_task(p.PA12, p.EXTI12)).unwrap();
    spawner.spawn(menu::middle_button_task(p.PA15, p.EXTI15)).unwrap();
    spawner.spawn(lcd::lcd_init_task(p.PC0, p.PC13, p.PC14, p.PC15)).unwrap();
    spawner.spawn(video_adc::video_adc_task(p.I2C1, p.PB8, p.PB9, p.DMA1_CH6, p.DMA1_CH3)).unwrap();
    spawner.spawn(switches::switches_task(p.PB7.degrade(), p.PB6.degrade(), p.PB5.degrade(), p.PB4.degrade())).unwrap();

    loop {
        Timer::after_millis(3000).await;
    }
}
