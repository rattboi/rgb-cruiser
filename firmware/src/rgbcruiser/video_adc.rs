use defmt::info;

use embassy_time::Timer;

use embassy_stm32::{bind_interrupts, peripherals};
use embassy_stm32::i2c;
use embassy_stm32::time::khz;

bind_interrupts!(struct Irqs {
    I2C1 => i2c::EventInterruptHandler<peripherals::I2C1>, i2c::ErrorInterruptHandler<peripherals::I2C1>;
});

#[embassy_executor::task]
pub async fn video_adc_task(periph: peripherals::I2C1, scl: peripherals::PB8, sda: peripherals::PB9, tx_dma: peripherals::DMA1_CH6, rx_dma: peripherals::DMA1_CH3) {
    const ADC_ADDRESS: u8 = 0x4C;
    const IDENT: u8 = 0x00;

    let mut i2c = i2c::I2c::new(periph, scl, sda, Irqs, tx_dma, rx_dma, khz(100), Default::default());

    let mut data = [0u8; 1];

    match i2c
        .write_read(ADC_ADDRESS, &[IDENT], &mut data)
        .await
    {
        Ok(()) => {
            info!("Read from i2c reg 0: {=u8:#x}", data[0]);
            match data[0] {
                0x21 => info!("Expected value read from ADC"),
                _    => info!("Oops, what is this? Not the THC7984")
            }
        }
        Err(e) => {
            info!("Couldn't read from i2c reg 0: {}", e);
        }
    }

    i2c.write_vectored(ADC_ADDRESS, &[
        // Set initial address to 0x01
        &[0x01],

        // Power / Enable
        &[0x14], // 01

        // Oversampling/ PLL Divider
        &[0x41], // 02
        &[0xA0], // 03

        // VCO/Charge Pump/Clock Source
        &[0x8C], // 04

        // Phase Adjust
        &[0x00], // 05

        // Red Gain
        &[0x04], // 06
        &[0x00], // 07

        // Green Gain
        &[0x04], // 08
        &[0x00], // 09

        // Blue Gain
        &[0x04], // 0A
        &[0x00], // 0B

        // Red Offset
        &[0xFF], // 0C
        &[0xF0], // 0D

        // Green Offset
        &[0xFF], // 0E
        &[0xF0], // 0F

        // Blue Offset
        &[0xFF], // 10
        &[0xF0], // 11

        // Input Port
        &[0x20], // 12

        // Sync POL
        &[0x00], // 13

        // HSOUT Settings
        &[0xFE], // 14
        &[0x20], // 15

        // Clmap Settings
        &[0x0A], // 16
        &[0x80], // 17
        &[0x08], // 18
        &[0x10], // 19

        // SOG Control
        &[0x64], // 1A

        // SOGOUT Control
        &[0x04], // 1B

        // Output Format
        &[0x04], // 1C

        // Drive Strength
        &[0x55], // 1D

        // Output Signal Control
        &[0x02], // 1E

        // HSync Filter
        &[0x13], // 1F

        // VSync Output Settings
        &[0x00], // 20
        &[0x04], // 21

        // PLL Coast
        &[0x06], // 22
        &[0x14], // 23

        // Clamp Pre/Post-Coast
        &[0x06], // 24
        &[0x14], // 25

        // DE Start Pos
        &[0x01], // 26
        &[0x70], // 27

        // DE Width
        &[0x05], // 28
        &[0x00], // 29

        // VBlank Frontporch/Backporch
        &[0x01], // 2A
        &[0x46], // 2B
    ]).await.unwrap();

    info!("A2D initialized");

    loop {
        Timer::after_millis(3000).await;
    }
}

