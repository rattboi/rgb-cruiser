use core::cmp;
use core::sync::atomic::Ordering;

use defmt::{info,debug};

use embassy_time::Instant;

use embassy_stm32::peripherals;
use embassy_stm32::exti::ExtiInput;
use embassy_stm32::gpio::Pull;

use crate::backlight::{PWM_SIGNAL,PWM_INTERVAL};

#[embassy_executor::task]
pub async fn up_button_task(button_pin: peripherals::PB3, exti: peripherals::EXTI3) {
    let mut button = ExtiInput::new(button_pin, exti, Pull::Up);

    loop {
        button.wait_for_rising_edge().await;

        let mut pwm_int = PWM_INTERVAL.load(Ordering::Relaxed);
        pwm_int = cmp::min(pwm_int, 4) + 1;
        PWM_INTERVAL.store(pwm_int, Ordering::Relaxed);

        info!("Up Button Pressed, sending {}", pwm_int);
        PWM_SIGNAL.signal(pwm_int);
    }
}

#[embassy_executor::task]
pub async fn down_button_task(button_pin: peripherals::PA12, exti: peripherals::EXTI12) {
    let mut button = ExtiInput::new(button_pin, exti, Pull::Up);

    loop {
        button.wait_for_rising_edge().await;

        let mut pwm_int = PWM_INTERVAL.load(Ordering::Relaxed);
        pwm_int = cmp::max(pwm_int, 1) - 1;
        PWM_INTERVAL.store(pwm_int, Ordering::Relaxed);

        info!("Down Button Pressed, sending {}", pwm_int);
        PWM_SIGNAL.signal(pwm_int);
    }
}

#[embassy_executor::task]
pub async fn middle_button_task(button_pin: peripherals::PA15, exti: peripherals::EXTI15) {
    let mut button = ExtiInput::new(button_pin, exti, Pull::Up);

    loop {
        button.wait_for_falling_edge().await;
        let start = Instant::now();
        button.wait_for_rising_edge().await;
        let end = Instant::now();
        let ms = end.duration_since(start).as_millis();

        if ms < 300 {
            debug!("short middle press, {}", ms)
        } else if ms < 1500 {
            debug!("middle middle press, {}", ms)
        } else {
            debug!("long middle press, {}", ms)
        }

        let pwm_int = 3;
        PWM_INTERVAL.store(pwm_int, Ordering::Relaxed);

        info!("Middle Button Pressed, sending {}", pwm_int);
        PWM_SIGNAL.signal(pwm_int);
    }
}
