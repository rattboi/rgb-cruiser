// switches.rs

use embassy_time::Timer;

use embassy_stm32::gpio::{AnyPin, Input, Pull};

#[embassy_executor::task]
pub async fn switches_task(sw1: AnyPin, sw2: AnyPin, sw3: AnyPin, sw4: AnyPin) {
    let mut _sw1_p = Input::new(sw1, Pull::Up);
    let mut _sw2_p = Input::new(sw2, Pull::Up);
    let mut _sw3_p = Input::new(sw3, Pull::Up);
    let mut _sw4_p = Input::new(sw4, Pull::Up);

    loop {
        Timer::after_millis(100).await;
    }
}