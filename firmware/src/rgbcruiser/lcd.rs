use embassy_time::Timer;

use embassy_stm32::peripherals;
use embassy_stm32::gpio::Pin;
use embassy_stm32::gpio::Flex;
use embassy_stm32::gpio::Speed;
use embassy_stm32::gpio::Level;
use embassy_stm32::gpio::Output;

use defmt::trace;
use defmt::info;

use crate::three_wire::{ThreeWireSerial,AnyPinWrapper,AsyncWrite,AsyncRead};

#[embassy_executor::task]
pub async fn lcd_init_task(reset_pin: peripherals::PC0, cs_pin: peripherals::PC13, sck_pin: peripherals::PC14, sda_pin: peripherals::PC15) {
    let mut lcd_nreset = Output::new(reset_pin, Level::High, Speed::VeryHigh);
    Timer::after_millis(100).await;
    lcd_nreset.set_low();
    Timer::after_millis(100).await;
    lcd_nreset.set_high();
    Timer::after_millis(100).await;

    let tw_cs = AnyPinWrapper(Flex::new(cs_pin.degrade()));
    let tw_sck = AnyPinWrapper(Flex::new(sck_pin.degrade()));
    let tw_sda = AnyPinWrapper(Flex::new(sda_pin.degrade()));

    let mut s = ThreeWireSerial::new(tw_cs, tw_sck, tw_sda, 100);

    Timer::after_millis(100).await;

    match test_for_lcd(&mut s).await {
        Ok(()) => {
            info!("lcd found, initializing...");
            lcd_init(&mut s).await;
        }

        Err(()) => {
            info!("lcd not found, NOT initializing...");
        }
    }

    loop {
        Timer::after_millis(3000).await;
    }
}

async fn lcd_init(s: &mut ThreeWireSerial<AnyPinWrapper,AnyPinWrapper,AnyPinWrapper>) {
    let init_data = [
        (0x10u8, 0x09u8), // Setup GRB and Power Mode
        (0x19, 0xEE),     // Set Display Mode
        (0x17, 0x45),     // Set HBLANK
        (0x18, 0x15),     // Set VBLANK
        (0x14, 0x48),     // Set Brightness
        (0x11, 0x40),     // Set Contrast Gain
        (0x19, 0xEE),     // Set Display Mode (again?)
    ];

    for command in init_data {
        s.write(command.0, command.1).await.unwrap();
    }
}


async fn test_for_lcd(s: &mut ThreeWireSerial<AnyPinWrapper,AnyPinWrapper,AnyPinWrapper>) -> Result<(), ()>  {
    let mut id_bytes = [0_u8; 3];
    
    for addr in 1..=3 {
        id_bytes[addr - 1] = s.read(addr as u8).await.unwrap();
    }

    trace!("id bytes: {=[u8; 3]:#x}", id_bytes);

    match &id_bytes.iter().all(|x| *x == 0x7F) {
        true => Ok(()),
        false => Err(())
    }
}
