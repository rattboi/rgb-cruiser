use core::sync::atomic::{AtomicU32, Ordering};

use defmt::info;

use embassy_sync::blocking_mutex::raw::ThreadModeRawMutex;
use embassy_sync::signal::Signal;

use embassy_stm32::peripherals;
use embassy_stm32::timer::Channel;
use embassy_stm32::timer::simple_pwm::{PwmPin,SimplePwm};
use embassy_stm32::time::khz;
use embassy_stm32::gpio::OutputType;

pub static PWM_SIGNAL: Signal<ThreadModeRawMutex, u32> = Signal::new();
pub static PWM_INTERVAL: AtomicU32 = AtomicU32::new(3);

#[embassy_executor::task]
pub async fn backlight_task(pwm_pin: peripherals::PA10, pwm_timer: peripherals::TIM22) {
    let ch2 = PwmPin::new_ch2(pwm_pin, OutputType::PushPull);
    let mut pwm = SimplePwm::new(pwm_timer, None, Some(ch2), None, None, khz(1), Default::default());

    let max = pwm.get_max_duty();
    pwm.enable(Channel::Ch2);
    info!("PWM initialized");
    info!("PWM max duty {}", max);

    let mut pwm_int = PWM_INTERVAL.load(Ordering::Relaxed);

    loop {
        let duty = (max / 5) * pwm_int;
        info!("PWM duty cycle {}", duty);
        pwm.set_duty(Channel::Ch2, duty);
        pwm_int = PWM_SIGNAL.wait().await;
    }
}