//! Software three-wire SPI communication

#![allow(dead_code)]

use defmt::{info,debug,trace};

use embassy_time::Duration;

use embassy_stm32::gpio::{Flex, Pull, Speed};

/// Serial communication error type
#[derive(Debug)]
#[derive(defmt::Format)]
pub enum ThreeWireError {
    /// Timeout
    Timeout,
}

pub trait IOPin {
    fn set_output(&mut self);
    fn set_input(&mut self);

    fn is_high(&mut self) -> bool;

    #[inline]
    fn is_low(&mut self) -> bool {!self.is_high()}

    fn set_high(&mut self);

    fn set_low(&mut self);

    fn set_open_drain(&mut self);
}

pub trait AsyncRead {
    /// Read error
    type Error;

    /// Reads a single word from the serial interface
    fn read(&mut self, addr: u8) -> impl core::future::Future<Output = Result<u8, Self::Error>> + Send;
}

pub trait AsyncWrite {
    /// Write error
    type Error;

    /// Writes a single word from the serial interface
    fn write(&mut self, addr: u8, word: u8) -> impl core::future::Future<Output = Result<(), Self::Error>> + Send;
}

/// Bit banging serial communication (three-wire)
pub struct ThreeWireSerial<CS,SCK,SDA>
    where
        CS: IOPin,
        SCK: IOPin,
        SDA: IOPin,
{
    cs: CS,
    sck: SCK,
    sda: SDA,

    // Bit rate period (1 / bauds)
    bit_period: Duration,
    // Read timeout in milliseconds.
    timeout_ms: Option<Duration>,
}

impl<CS,SCK,SDA> ThreeWireSerial<CS,SCK,SDA>
    where
        CS: IOPin,
        SCK: IOPin,
        SDA: IOPin,
{
    pub fn new(mut cs: CS, mut sck: SCK, mut sda: SDA, baud_rate: u32) -> Self {

        debug!("ThreeWireSerial init at {} baud rate", baud_rate);

        cs.set_output();
        cs.set_high();

        sck.set_output();
        sck.set_high();

        sda.set_output();
        sda.set_low();

        Self { cs, sck, sda, bit_period: Duration::from_hz(baud_rate as u64), timeout_ms: None }
    }

    pub fn word_transfer_duration(&self) -> Duration {
        16 * self.bit_period
    }

    /// the timeout for R/W operation
    pub fn set_timeout(&mut self, timeout_ms: Option<Duration>) {
        self.timeout_ms = timeout_ms;
    }
}

impl<CS,SCK,SDA> AsyncWrite for ThreeWireSerial<CS,SCK,SDA>
    where
        CS: IOPin + Send,
        SCK: IOPin + Send,
        SDA: IOPin + Send,
{
    type Error = ThreeWireError;

    async fn write(&mut self, addr: u8, data: u8) -> Result<(), Self::Error> {
        info!(">: {:08b} -> {:08b}", addr, data);

        let mut ticker = embassy_time::Ticker::every(self.bit_period);

        self.cs.set_low();  
        ticker.next().await;

        let mut data_out = addr;

        for _bit in 0..8 {
            self.sck.set_low();
            ticker.next().await;
            if data_out & 0x80 == 0x80 {
                trace!("W {:08b} [1]", data_out);
                self.sda.set_high();
            } else {
                trace!("W {:08b} [0]", data_out);
                self.sda.set_low();
            }
            data_out <<= 1;
            ticker.next().await;
            self.sck.set_high();
            ticker.next().await;
        }

        // wait a tick (or 8)
        for _wait in 0..8 {
            ticker.next().await;
        }

        let mut data_out = data;

        for _bit in 0..8 {
            self.sck.set_low();
            ticker.next().await;
            if data_out & 0x80 == 0x80 {
                trace!("W {:08b} [1]", data_out);
                self.sda.set_high();
            } else {
                trace!("W {:08b} [0]", data_out);
                self.sda.set_low();
            }
            data_out <<= 1;
            ticker.next().await;
            self.sck.set_high();
            ticker.next().await;
        }

        // one extra for good measure
        ticker.next().await;
        self.cs.set_high();
        ticker.next().await;
        Ok(())
    }
}

impl<CS,SCK,SDA> AsyncRead for ThreeWireSerial<CS,SCK,SDA>
    where
        CS: IOPin + Send,
        SCK: IOPin + Send,
        SDA: IOPin + Send,
{
    type Error = ThreeWireError;

    async fn read(&mut self, addr: u8) -> Result<u8, Self::Error> {

        let mut ticker = embassy_time::Ticker::every(self.bit_period);

        self.cs.set_low();  
        ticker.next().await;

        let mut data_out = addr | 0x80; // set top bit to mark addr as a read

        for _bit in 0..8 {
            self.sck.set_low();
            ticker.next().await;
            if data_out & 0x80 == 0x80 {
                trace!("W {:08b} [1]", data_out);
                self.sda.set_high();
            } else {
                trace!("W {:08b} [0]", data_out);
                self.sda.set_low();
            }
            data_out <<= 1;
            ticker.next().await;
            self.sck.set_high();
            ticker.next().await;
        }

        let mut data_in = 0;

        self.sda.set_input();

        for _wait in 0..8 {
            ticker.next().await;
        }

        // Read 8 bits
        for _bit in 0..8 {
            self.sck.set_low();
            ticker.next().await;

            data_in <<= 1;
            if self.sda.is_high() {
                data_in |= 1;
                trace!("R [#{}] {:08b} [1]", _bit, data_in);
            } else {
                trace!("R [#{}] {:08b} [0]", _bit, data_in);
            }

            self.sck.set_high();
            ticker.next().await;
        }

        // one extra for good measure
        ticker.next().await;
        self.cs.set_high();
        self.sda.set_output();

        Ok(data_in)
    }
}

pub struct AnyPinWrapper(pub Flex<'static>);

impl IOPin for AnyPinWrapper
{
    #[inline]
    fn set_output(&mut self) {
        self.0.set_as_output(Speed::VeryHigh);
    }
    #[inline]
    fn set_input(&mut self) {
        self.0.set_as_input(Pull::Up);
    }
    #[inline]
    fn is_high(&mut self) -> bool {
        self.0.is_high()
    }
    #[inline]
    fn set_high(&mut self) { self.0.set_high() }
    #[inline]
    fn set_low(&mut self) { self.0.set_low() }
    #[inline]
    fn set_open_drain(&mut self) { self.0.set_as_input_output(Speed::VeryHigh) }
}
