Id,Time[ns],0:I²C: Address/Data
1,36464970.00,Start
2,36475390.00,Address write: 98
3,36548030.00,Write
4,36558410.00,ACK

5,36568790.00,Data write: 01 ( base address 1)
; 0x01 Power / Enable
7,36662220.00,Data write: 14 
00010100 ->
  Chip Power-On: Power-On
  Auto Output Enable: Disable
  Output Enable: Enable
  SOGOUT Output Enable: Disable

; 0x02 Oversampling / PLL Divider Ratio
9,36755620.00,Data write: 41 
01000001

; 0x03 PLL Divider LSB
11,36849030.00,Data write: A0
01000001:10100000 (0x02:0x03)
x
 10               -> 4x Oversampling
   00001101000000 -> Pll Divider Ratio

; 0x04 VCO/Charge Pump/Clock Source
13,36942460.00,Data write: 8C
10001100
1        -> reserved
 00      -> VCO Frequency Range: 1/8
   011   -> 250uA
      00 -> Sampling Clock

; 0x05 Phase Adjust
15,37035870.00,Data write: 00
00 -> 
  no phase adjust

; 0x06 red gain MSB
17,37129350.00,Data write: 04 
; 0x07 red gain LSB
19,37222780.00,Data write: 00
xxxxx100:00000000 (default) Gain: 1.0


; 0x08 green gain MSB
21,37316180.00,Data write: 04
; 0x09 green gain LSB
23,37409620.00,Data write: 00
xxxxx100:00000000 (default) Gain: 1.0

; 0x0A blue gain MSB
25,37503090.00,Data write: 04
; 0x0B blue gain LSB
27,37596520.00,Data write: 00
xxxxx100:00000000 (default) Gain: 1.0

; 0x0C red offset MSB
29,37689950.00,Data write: FF
; 0x0D red offset LSB
31,37783340.00,Data write: F0
xxxxxxx1:11110000

; 0x0E green offset MSB
33,37876740.00,Data write: FF
; 0x0F green offset LSB
35,37970150.00,Data write: F0
xxxxxxx1:11110000

; 0x10 blue offset MSB
37,38063610.00,Data write: FF
; 0x11 blue offset LSB
39,38157010.00,Data write: F0
xxxxxxx1:11110000

; 0x12 input port
41,38250460.00,Data write: 20
00100000
00       -> reserved
  1      -> Input Port Auto Select: Enable
   0     -> reserved
    0    -> Input Port: Port-0
     0   -> Sync Type Auto Select: Enable
      00 -> Sync Type Select: Separate Sync

; 0x13 Sync POL
43,38343900.00,Data write: 00
0        -> xxx
 0       -> H/VSync Input Polarity Auto: no
  0      -> HSync Input Polarity: Active-Low
   0     -> VSync Input Polarity: Active-Low
    0    -> H/VSync Output Polarity Auto: no
     0   -> HSOUT Polarity: Active-Low
      0  -> VSOUT Polarity: Active-Low
       0 -> VSOUT Interlace Mode: Disable
; 0x14 HSOUT Start Position (2s complement)
45,38437330.00,Data write: FE 
(-2?)

; 0x15 HSOUT Pulse Width
47,38530770.00,Data write: 20
20 pixels

; 0x16 Clamp / Coast
49,38624200.00,Data write: 0A
00001010
xxx      -> doesn't matter
   0     -> PLL COAST Source: Internal PLL Coast
    1    -> PLL/Clamp COAST Input Polarity: doesn't matter, because coast source is internal
     0   -> Clamp Pulse Source: Internal Clamp Pulse
      1  -> Clamp Pulse Input Polarity: doesn't matter, because clamp pulse source is internal
       0 -> Clamp COAST Source: Internal Clamp Coast

; 0x17 Clamp Mode
51,38717610.00,Data write: 80
10000000
x        -> doesn't matter
 0       -> Clamp Pulse Start Reference Edge: leading edge of HSync input
  00     -> R-ch Clamp Mode: Pedestal Clamp
    00   -> G-ch Clamp Mode: Pedestal Clamp
      00 -> B-ch Clamp Mode: Pedestal Clamp

; 0x18 Clamp Pulse Start Pos (1 pixel steps)
53,38811050.00,Data write: 08

; 0x19 Clamp Pulse Width (1 pixel steps)
55,38904450.00,Data write: 10

; 0x1A SOG Control
57,38997890.00,Data write: 64
01100100
x
 1       -> SOG Slicer Hysterisis: Enable
  10     -> SOG Input Filter: Reserved?
    0100 -> SOG Slicer Threshold: 4x 15mV above the Sync Tip

; 0x1B SOGOUT Control
59,39091290.00,Data write: 04
00000100
0        -> SOGOUT Output Polarity: Active-Low
 00      -> SOGOUT Output Signal: Raw HSYNC
   00100 -> Preamp Bandwidth: LPF 

; 0x1C Output Format
61,39184730.00,Data write: 04
00000100
00       -> Output Format: 4:4:4
  0      -> 4:2:2 Decimation Filter: Disable
   00    -> Output Clock DATACK: Pixel Clock
     100 -> Output Clock Phase: 4/8T delay

; 0x1D Drive Strength
63,39278110.00,Data write: 55
01010101
01       -> reserved
  01     -> RGB Data Output Drive Strength: Medium
    01   -> Sync Output Drive Strength: Medium
      01 -> Clock Output Drive Strength: Medium

; 0x1E Output Signal Control
65,39371520.00,Data write: 02
00000010
00       -> HSOUT Output Signal: HO
  00     -> VSOUT Output Signal: VO
    001  -> O/E Field Output Signal: Regenerated Field
       0 -> O/E Field Polarity: Odd low/Even high

; 0x1F HSync Filter
67,39464950.00,Data write: 13
00010011
0        -> don't care
 0       -> reserved (0)
  0      -> reserved (0)
   1     -> PLL HSYNC Filter: Enable
    0011 -> HSYNC Filter Window Width: +/-400ns 

; 0x20 VSync Output Timing/Start Pos
69,39558360.00,Data write: 00
00000000
0        -> VSync Output Timing Auto Setting: Disable
 0000000 -> 0 start pos

; 0x21 VSync Output Pulse Width (1 line steps)
71,39651820.00,Data write: 04 
4 lines

; 0x22 PLL COAST Auto/Pre-Coast
73,39745240.00,Data write: 06
00000110
0        -> PLL COAST Auto Timing: Disable
 0000110 -> Start Pos: 0 lines

; 0x23 PLL COAST Post-Coast
75,39838700.00,Data write: 14
00010100 -> End Pos: 20 lines(?)

; 0x24 Clamp Pre-Coast
77,39932140.00,Data write: 06
6 lines start pos

; 0x25 Clamp Post-Coast
79,40025570.00,Data write: 14
20 lines end pos

; 0x26 DE Start Pos MSB
81,40119040.00,Data write: 01

; 0x27 DE Start Pos LSB
83,40212470.00,Data write: 70
00000001:01110000 -> 368 pixels
DE Start Pos after leading edge of HSync

; 0x28 DE Width MSB
85,40305890.00,Data write: 05
; 0x29 DE Width LSB
87,40399260.00,Data write: 00
00000101:00000000 -> 1280 pixels

; 0x2A V-Blank Front Porch (DE Low Start Pos)
89,40492690.00,Data write: 01
1 line vblank start position prior to leading edge of VSync

; 0x2B V-Blank Back Porch
91,40586100.00,Data write: 46
70 lines vblank end position after trailing edge of VSync

93,40683710.00,Stop

// later
// based on width saved in OSD
94,43465870.00,Start
95,43476310.00,Address write: 98
96,43548990.00,Write
97,43559370.00,ACK
98,43569740.00,Data write: 02
99,43652790.00,ACK
100,43663210.00,Data write: 41
101,43746250.00,ACK
102,43756650.00,Data write: 90
103,43839710.00,ACK
104,43854330.00,Stop


// later
// based on phase saved in OSD
105,43863770.00,Start
106,43874210.00,Address write: 98
107,43946900.00,Write
108,43957270.00,ACK
109,43967650.00,Data write: 05
110,44050740.00,ACK
111,44061110.00,Data write: 28
112,44144230.00,ACK
113,44158820.00,Stop
